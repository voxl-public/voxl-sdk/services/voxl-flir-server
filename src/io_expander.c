/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <voxl_io/i2c.h>


#include "io_expander.h"
#include "config_file.h"

// M0187 uses PI4IOE5V6408ZTAEX
// M0188 uses TCA9543A
#define EXPANDER_TYPE_NONE     0x00 // 00000000
#define EXPANDER_TYPE_PI4IO    0x01 // 00000001
#define EXPANDER_TYPE_TCA9543A 0x02 // 00000010

static int is_enabled = 0;
static int expander_type = EXPANDER_TYPE_NONE;

// PI4IOE5V6408ZTAEX ----------------------------------------------------------

// hard coded values for M0187 lepton breakout board

// bitshift right since qcom i2c driver is going to <<1 later
#define PI4IO_ADDRESS (0x86>>1) // ADDR pin pulled low, address is 0b1000011x
//#define PI4IO_ADDRESS (0x88>>1) // ADDR pin pulled high address is 0b1000100x

#define PI4IO_CHIP_ID 0x1
#define PI4IO_IO_DIRECTION 0x3
#define PI4IO_OUTPUT 0x5
#define PI4IO_OUTPUT_HI_IMPEDANCE 0x7
#define PI4IO_INPUT_STATUS 0xF
#define PI4IO_INTERRUPT_STATUS 0x13

#define PI4IO_CHIP_ID_VAL 0xA0
#define PI4IO_CHIP_ID_MASK 0xFC


// TCA9543A -------------------------------------------------------------------

#define TCA9543A_ADDRESS (0x73)        // A0 = 1, A1 = 1
#define TCA9543A_CONTROL_REGISTER 0x01
#define TCA9543A_CHANNEL_0_ENABLE 0x01 // M0188 - J10
#define TCA9543A_CHANNEL_1_ENABLE 0x02 // M0188 - J5

// ----------------------------------------------------------------------------

int io_expander_init(void)
{
	// Example Use Cases:
	// VOXL2      --> M0173 interposer            --> M0157 Lepton Breakout
	// VOXL2      --> M0173 interposer            --> M0187 Lepton Breakout (PI4IO)
	// VOXL2      --> M0130 add on                --> M0157 Lepton Breakout
	// VOXL2      --> M0130 add on                --> M0187 Lepton Breakout (PI4IO)
	// VOXL2 Mini --> M0188 interposer (TCA9543A) --> M0157 Lepton Breakout
	// VOXL2 Mini --> M0188 interposer (TCA9543A) --> M0187 Lepton Breakout (PI4IO)

	uint8_t val;
	// i2c not configured, can't use io expander even if present
	if(!en_i2c)return -1;

	expander_type = EXPANDER_TYPE_NONE;

	// M0188 - Try to communicate with TCA9543A first, M0187 could be 'behind' M0188
	printf("Attempting intitialization for M0188 (TCA9543A)\n");
	if(voxl_i2c_init(i2c_bus, TCA9543A_ADDRESS)){
		fprintf(stderr, "ERROR in %s, failed to init i2c bus for TCA9543A\n", __FUNCTION__);
	} else {
		// fetch the control register
		if(voxl_i2c_read_byte(i2c_bus, TCA9543A_CONTROL_REGISTER, &val)!=1){
			fprintf(stderr, "%s, failed to read TCA9543APWR control register\n", __FUNCTION__);
			fprintf(stderr, "If you are not using an M0188 lepton board then this is expected\n");
		} else {	
			// write to comtrol resgister to enable channel 0
			if(voxl_i2c_write_byte(i2c_bus, TCA9543A_CONTROL_REGISTER, TCA9543A_CHANNEL_0_ENABLE)){
				fprintf(stderr, "ERROR in %s, failed to write to TCA9543A status register\n", __FUNCTION__);
				return -1;
			}

			printf("Using M0188 (TCA9543A) - channel 0 enabled\n");
			expander_type |= EXPANDER_TYPE_TCA9543A;
			is_enabled = 1;
		}
	}

	// M0187 - Try to communicate with PI4IOE5V6408ZTAEX
	printf("Attempting intitialization for M0187 (PI4IO)\n");

	// calling voxl_i2c_init after initial init will set new address
	if(voxl_i2c_init(i2c_bus, PI4IO_ADDRESS)){
		fprintf(stderr, "ERROR in %s, failed to init i2c bus for PI4IO\n", __FUNCTION__);
	} else {
		if(voxl_i2c_read_byte(i2c_bus, PI4IO_CHIP_ID, &val)!=1){
			fprintf(stderr, "%s, failed to read from io expander\n", __FUNCTION__);
			fprintf(stderr, "If you are not using an M0187 lepton board then this is expected\n");
		}
		else {
			// fetch the chip ID register to check we are talking to the right thing
			uint8_t val;
			if(voxl_i2c_read_byte(i2c_bus, PI4IO_CHIP_ID, &val)!=1){
				fprintf(stderr, "%s, failed to read from io expander\n", __FUNCTION__);
				fprintf(stderr, "If you are not using an M0188 lepton board then this is expected\n");
				return -1;
			}
			//printf("0x%2x\n", val);

			if((val & PI4IO_CHIP_ID_MASK) != PI4IO_CHIP_ID_VAL) {
				fprintf(stderr, "ERROR in %s, chip id does not match\n", __FUNCTION__);
				return -1;
			}

			// set pin 0 high first so the pin doesn't toggle when enabling it
			if(voxl_i2c_write_byte(i2c_bus, PI4IO_OUTPUT, 0x01)){
				fprintf(stderr, "ERROR in %s, failed to write to io expander output register\n", __FUNCTION__);
				return -1;
			}

			// set pin 0 as output
			if(voxl_i2c_write_byte(i2c_bus, PI4IO_IO_DIRECTION, 0x01)){
				fprintf(stderr, "ERROR in %s, failed to write to io expander io direction reg\n", __FUNCTION__);
				return -1;
			}

			// disable high impedance
			if(voxl_i2c_write_byte(i2c_bus, PI4IO_OUTPUT_HI_IMPEDANCE, ~0x01)){
				fprintf(stderr, "ERROR in %s, failed to write to io expander output high-impedance reg\n", __FUNCTION__);
				return -1;
			}

			printf("Using M0187 (PI4IO)\n");
			expander_type |= EXPANDER_TYPE_PI4IO;
			is_enabled = 1;
		}
	}
	
	return 0;
}

int io_expander_is_enabled(void)
{
	return is_enabled;
}


int io_expander_toggle_reset(void)
{
	// exit quietly, we can still run without the io-expander
	if(!is_enabled) return -1;

	if(expander_type & EXPANDER_TYPE_PI4IO){

		printf("hard-resetting lepton on M0187\n");

		// pull pin low
		if(voxl_i2c_write_byte(i2c_bus, PI4IO_OUTPUT, 0x00)){
			fprintf(stderr, "ERROR in %s, failed to write to io expander output register\n", __FUNCTION__);
			return -1;
		}

		// TODO figure out an appropriate sleep
		usleep(100);


		// raise pin again
		if(voxl_i2c_write_byte(i2c_bus, PI4IO_OUTPUT, 0x01)){
			fprintf(stderr, "ERROR in %s, failed to write to io expander output register\n", __FUNCTION__);
			return -1;
		}
	}

	return 0;
}
