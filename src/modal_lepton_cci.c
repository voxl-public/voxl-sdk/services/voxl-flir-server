

#include <stdio.h>
#include <unistd.h>

#include "lepton_sdk/LEPTON_SDK.h"
#include "lepton_sdk/LEPTON_SYS.h"
#include "lepton_sdk/LEPTON_OEM.h"
#include "lepton_sdk/LEPTON_Types.h"

#include "modal_lepton_cci.h"
#include "common.h"
#include "autopilot_monitor.h"
#include <rc_math/other.h>
#include "parser.h"
#include "config_file.h"
#include "io_expander.h"

static int i2c_connected;

static LEP_CAMERA_PORT_DESC_T _port;

static shutter_mode_t current_shutter_mode;
static int64_t t_last_shutter = 0;
static int shutter_mode_set_flag = 0;



int modal_lepton_connect(void)
{
	int ret = LEP_OpenPort(i2c_bus, LEP_CCI_TWI, 400, &_port);
	if(ret){
		fprintf(stderr, "failed to open I2C port %d, disabling CCI ret=%d\n", i2c_bus, ret);
		i2c_connected = 0;
		return ret;;
	}
	printf("successfully opened I2C port\n");

	if(LEP_RunSysPing(&_port)){
		fprintf(stderr, "failed to ping lepton on I2C, trying again\n");
		usleep(500000);
		if(LEP_RunSysPing(&_port)){
			fprintf(stderr, "failed to ping lepton on I2C a second time\n");
			i2c_connected = 0;
			return -1;
		}
	}

	i2c_connected = 1;
	return 0;
}

int modal_lepton_run_ffc(void)
{
	if(!i2c_connected) return -1;

	int ret = LEP_RunSysFFCNormalization(&_port);
	if(ret){
		fprintf(stderr, "ERROR: FAILED TO RUN MANUAL SHUTTER\n");
		return -1;
	}
	t_last_shutter = rc_time_monotonic_ns();
	return 0;
}

// t_last_shutter set by manual shutter and when reading fcc state through CCI
double modal_lepton_s_since_shutter(void)
{
	return ((double)(rc_time_monotonic_ns()-t_last_shutter)/1000000000.0);

}

float modal_lepton_get_aux_temperature_celsius(void)
{
	LEP_SYS_AUX_TEMPERATURE_CELCIUS_T auxTemperature;	
	if(LEP_GetSysAuxTemperatureCelcius(&_port, &auxTemperature)){
		return -300.0;
	}

	// printf("aux temperature %f\n", (double)auxTemperature);
	return auxTemperature;
}

float modal_lepton_get_fpa_temperature_celsius(void)
{
	LEP_SYS_FPA_TEMPERATURE_CELCIUS_T fpaTemperature;	
	if(LEP_GetSysFpaTemperatureCelcius(&_port, &fpaTemperature)){
		return -300.0;
	}

	// printf("fpa temperature %f\n", (double)fpaTemperature);
	return fpaTemperature;
}

int modal_lepton_reboot(void)
{
	if(io_expander_is_enabled()){
		io_expander_toggle_reset();
	}
	else if(!i2c_connected) return -1;


	global_reboot_cnt++;
	shutter_mode_set_flag = 0;
	if(en_debug_ffc) printf("rebooting lepton\n");

	if(!io_expander_is_enabled()){
		int ret = LEP_RunOemReboot(&_port);
		if(ret == LEP_ERROR_I2C_FAIL && en_debug_ffc){
			printf("WARNING: lepton reported I2C error, this is OK (probly)\n");
		}
		else if(ret){
			printf("ERROR: FAILED TO REBOOT ret = %d\n", ret);
			return ret;
		}
	}

	inform_parser_of_reboot();

	// Don't sleep here, let the caller decide if they want to sleep or not
	return 0;
}


int modal_lepton_get_and_set_ffc_profile(int en_in_air)
{
	if(en_debug_ffc) fprintf(stderr, "reading FFC shutter info\n");

	LEP_SYS_FFC_SHUTTER_MODE_OBJ_T mode_obj;
	if(LEP_GetSysFfcShutterModeObj(&_port, &mode_obj)){
		fprintf(stderr, "failed to get ffc shutter mode obj\n");
		return -1;
	}
	LEP_OEM_SHUTTER_PROFILE_OBJ_T profile_obj;
	if(LEP_GetOemShutterProfileObj(&_port, &profile_obj)){
		fprintf(stderr, "failed to get ffc shutter profile obj\n");
		return -1;
	}

	if(en_debug_ffc){
		switch(mode_obj.shutterMode){
		case LEP_SYS_FFC_SHUTTER_MODE_MANUAL:
			printf("read LEP_SYS_FFC_SHUTTER_MODE_MANUAL\n");
			break;
		case LEP_SYS_FFC_SHUTTER_MODE_AUTO:
			printf("read LEP_SYS_FFC_SHUTTER_MODE_AUTO\n");
			break;
		default:
			printf("read unknown shutter mode: %d\n", mode_obj.shutterMode);
		}

		printf("it's been %5.1fs since last ffc\n", modal_lepton_s_since_shutter());
		printf("ffc period: %6.1fs  ", (double)mode_obj.desiredFfcPeriod/1000.0);
		printf("ffc Temp delta: %4.1fC\n", (double)mode_obj.desiredFfcTempDelta/100.0);
		printf("closed frames: %d  ", profile_obj.closePeriodInFrames);
		printf("open   frames: %d\n", profile_obj.openPeriodInFrames);
	}

	// record the shutter time
	t_last_shutter = rc_time_monotonic_ns() - (mode_obj.elapsedTimeSinceLastFfc*1e6);


	// set up fcc params for default or fast modes
	if(en_in_air){
		if(en_debug_ffc) printf("setting shutter profile to fast values\n");
		profile_obj.closePeriodInFrames = closePeriodInFramesInAir;
		profile_obj.openPeriodInFrames = openPeriodInFramesInAir;
		mode_obj.desiredFfcPeriod = desiredFfcPeriodMsInAir;
		mode_obj.desiredFfcTempDelta = desiredFfcTempDeltaCentiDegInAir;
	}
	else{
		if(en_debug_ffc) printf("setting shutter profile to defaults\n");
		profile_obj.closePeriodInFrames = 4; // lepton default
		profile_obj.openPeriodInFrames = 1;  // lepton default
		mode_obj.desiredFfcPeriod = 180000;  // lepton default
		mode_obj.desiredFfcTempDelta = 150;  // lepton default
	}


	// write new values back
	if(LEP_SetSysFfcShutterModeObj(&_port, mode_obj)){
		fprintf(stderr, "WARNING: failed to set ffc shutter mode obj\n");
	}

	if(LEP_SetOemShutterProfileObj(&_port, profile_obj)){
		fprintf(stderr, "failed to set ffc shutter profile obj\n");
		return -1;
	}

	return 0;
}


int modal_lepton_set_ffc_manual(void)
{
	if(!i2c_connected) return -1;

	if(en_debug_ffc) fprintf(stderr, "Setting FFC shutter to manual mode\n");
	LEP_SYS_FFC_SHUTTER_MODE_OBJ_T obj;
	if(LEP_GetSysFfcShutterModeObj(&_port, &obj)){
		fprintf(stderr, "failed to get ffc shutter mode obj\n");
		return -1;
	}

	obj.shutterMode = LEP_SYS_FFC_SHUTTER_MODE_MANUAL;

	if(LEP_SetSysFfcShutterModeObj(&_port, obj)){
		fprintf(stderr, "failed to set ffc shutter mode\n");
		return -1;
	}
	if(en_debug_ffc) fprintf(stderr, "Successfully set FFC shutter to manual mode\n");
	current_shutter_mode = SHUTTER_MODE_MANUAL;
	shutter_mode_set_flag = 1;
	return 0;
}

int modal_lepton_set_ffc_auto(void)
{
	if(!i2c_connected) return -1;

	if(en_debug_ffc) fprintf(stderr, "Setting FFC shutter to auto mode\n");
	LEP_SYS_FFC_SHUTTER_MODE_OBJ_T obj;
	if(LEP_GetSysFfcShutterModeObj(&_port, &obj)){
		fprintf(stderr, "failed to get ffc shutter mode obj\n");
		return -1;
	}

	obj.shutterMode = LEP_SYS_FFC_SHUTTER_MODE_AUTO;

	if(LEP_SetSysFfcShutterModeObj(&_port, obj)){
		fprintf(stderr, "failed to set ffc shutter mode\n");
		return -1;
	}
	if(en_debug_ffc) fprintf(stderr, "Successfully set FFC shutter to auto mode\n");
	current_shutter_mode = SHUTTER_MODE_AUTO;
	shutter_mode_set_flag = 1;
	return 0;
}

int modal_lepton_set_ffc_flow(void)
{
	if(!i2c_connected) return -1;

	connect_to_autopilot();

	if(en_debug_ffc) fprintf(stderr, "Setting FFC shutter to flow mode\n");
	LEP_SYS_FFC_SHUTTER_MODE_OBJ_T obj;
	if(LEP_GetSysFfcShutterModeObj(&_port, &obj)){
		fprintf(stderr, "failed to get ffc shutter mode obj\n");
		return -1;
	}
	obj.shutterMode = LEP_SYS_FFC_SHUTTER_MODE_AUTO;

	if(LEP_SetSysFfcShutterModeObj(&_port, obj)){
		fprintf(stderr, "failed to set ffc shutter mode\n");
		return -1;
	}
	if(en_debug_ffc) fprintf(stderr, "Successfully set FFC shutter to flow mode\n");
	current_shutter_mode = SHUTTER_MODE_FLOW;
	shutter_mode_set_flag = 1;
	return 0;
}

int modal_lepton_has_set_shutter_mode_since_reset(void)
{
	return shutter_mode_set_flag;
}


shutter_mode_t modal_lepton_get_current_shutter_mode(void)
{
	return current_shutter_mode;
}


// put the shutter mode back to whatever it was before a reboot
void modal_lepton_reset_shutter_mode(void)
{
	switch(current_shutter_mode){
		case SHUTTER_MODE_AUTO:
			modal_lepton_set_ffc_auto();
			break;
		case SHUTTER_MODE_MANUAL:
			modal_lepton_set_ffc_manual();
			break;
		case SHUTTER_MODE_FLOW:
			modal_lepton_set_ffc_flow();
			break;
		default:
			fprintf(stderr, "ERROR in %s, unknown shutter mode\n", __FUNCTION__);
			break;
	}

	return;
}

